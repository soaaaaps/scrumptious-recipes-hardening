from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView,DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from pprint import pprint


from meal_plans.models import MealPlan



class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 2

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

class MealPlanCreateView(LoginRequiredMixin,CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "recipes", "date",]
    success_url = reverse_lazy("meal_plans_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)

    # def form_valid(self,form):
    #     form.instance.owner=self.request.user
    #     return super().form_valid(form)

class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

class MealPlanDeleteView(LoginRequiredMixin,DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")

class MealPlanUpdateView(LoginRequiredMixin,UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name","date","recipes"]
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        queryset= super().get_queryset()
        print(queryset)
        return queryset

    def get_context_data(self,**kwargs):
        context=super().get_context_data(**kwargs)
        pprint(context)
        return context
