# Generated by Django 4.0.3 on 2022-09-01 01:05

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recipes', '0008_recipe_author'),
        ('meal_plans', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mealplan',
            name='owner',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='mealplans', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='mealplan',
            name='recipes',
            field=models.ManyToManyField(blank=True, related_name='meal_plans', to='recipes.recipe'),
        ),
    ]
